package com.example.tarea1.model;

import lombok.Getter;

@Getter
public enum Year {
    YEAR_2015("2015"),
    YEAR_2016("2016"),
    YEAR_2017("2017"),
    YEAR_2018("2018"),
    YEAR_2019("2019"),
    YEAR_2020("2020"),
    YEAR_2021("2021"),
    YEAR_2022("2022"),
    YEAR_2023("2023");

    public final String year;

    Year(String year) {
        this.year = year;
    }

    public String getValue() {
        return this.year;
    }
}
