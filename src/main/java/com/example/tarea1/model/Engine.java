package com.example.tarea1.model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Getter
public enum Engine {
    SEDAN_ENGINE_1_4cc("1.4cc"),
    SEDAN_ENGINE_1_6cc("1.6cc"),
    SEDAN_ENGINE_2_0cc("2.0cc"),
    VAN_ENGINE_2_4cc("2.4cc"),
    VAN_ENGINE_3_0cc("3.0cc"),
    VAN_ENGINE_4_0cc("4.0cc"),
    SUV_ENGINE_1_8cc("1.8cc"),
    SUV_ENGINE_2_0cc("2.2cc"),
    SUV_ENGINE_2_4cc("2.8cc"),
    ;
    private static final Random random = new Random();
    private final String engine;
    Engine(String engine) {
        this.engine = engine;
    }

    public static String getRandomEngineByBrand(String keyword) {
        Engine[] allEngines = Engine.values();
        List<Engine> matchingEngines = new ArrayList<>();

        // Buscar motores que contengan la palabra clave
        for (Engine engine : allEngines) {
            if (engine.name().toLowerCase().contains(keyword.toLowerCase())) {
                matchingEngines.add(engine);
            }
        }

        if (!matchingEngines.isEmpty()) {
            Engine randomEngine = matchingEngines.get(random.nextInt(matchingEngines.size()));
            return randomEngine.getEngine();
        } else {
            return null; // No se encontraron motores que coincidan con la palabra clave
        }
    }

}
