package com.example.tarea1.model;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

@Data
@Entity
public class Vehicle {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private int id;
        private String brand;
        private String year;
        private String color;
        private int price;
        private boolean turbo;
        private String vehicleType;
        private Integer popularity=0;

        public String getVehicleType(){
                return this.getClass().getSimpleName();
        }
        public void increasePopularity(){
                this.popularity++;
        }


}

