package com.example.tarea1.model;

import jakarta.persistence.Entity;
import lombok.Data;

@Data
@Entity
public class Sedan extends Vehicle {
    private String engine;
}
