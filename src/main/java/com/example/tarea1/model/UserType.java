package com.example.tarea1.model;

import lombok.Getter;

@Getter
public enum UserType {

    User, Agent;
}
