package com.example.tarea1.model;

import jakarta.persistence.Entity;
import lombok.Data;

@Data
@Entity

public class VAN extends Vehicle {

    private boolean cabins;
    private String engine;
}
