package com.example.tarea1.model;

public enum Color {
    White,
    Blue,
    Red,
    Black,
    Gray
}
