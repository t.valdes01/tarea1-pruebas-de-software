package com.example.tarea1.model;

import lombok.Data;

@Data
public class User {
    private String userType;

    public User(String userType) {
        this.userType = userType;
    }
}
