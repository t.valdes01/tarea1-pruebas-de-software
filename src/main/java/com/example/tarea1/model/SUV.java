package com.example.tarea1.model;

import jakarta.persistence.Entity;
import lombok.Data;

@Data
@Entity
public class SUV  extends Vehicle{
    private String engine;
    private boolean sunroof;
}
