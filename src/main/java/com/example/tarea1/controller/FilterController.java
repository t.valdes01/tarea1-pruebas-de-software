package com.example.tarea1.controller;

import com.example.tarea1.model.Vehicle;
import com.example.tarea1.service.FilterService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/filter")
public class FilterController {

    private final FilterService filterService;

    public FilterController(FilterService filterService) {
        this.filterService = filterService;
    }

    @ApiResponse(description = "Filter vehicles by price, vehicle type, color and user type")
    @GetMapping("/filteredVehicles")
    public List<Vehicle> filter(
            @Parameter(description = "Price of the vehicle")
            @RequestParam(value = "price" , required = false) Integer price,

            @Parameter(description = "Type of the vehicle")
            @RequestParam(value = "vehicleType", required = false) String vehicleType,

            @Parameter(description = "Color of the vehicle")
            @RequestParam(value = "color", required = false) String color,

            @Parameter(description = "Type of the user (agent, user)")
            @RequestParam("user Type") String userType) {
        return filterService.filterVehicles(price, vehicleType, color, userType);

    }
}
