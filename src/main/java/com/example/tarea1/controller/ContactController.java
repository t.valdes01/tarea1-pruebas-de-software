package com.example.tarea1.controller;

import com.example.tarea1.model.Vehicle;
import com.example.tarea1.service.ContactService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/contact")
public class ContactController {

    private final ContactService contactService;

    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }


    @ApiResponse(description = "Consult an specific vehicle")
    @GetMapping("/vehicleConsultation")
    public Vehicle vehicleConsultation(
            @Parameter(description = "Id of the vehicle")
            @RequestParam("id") int id){
        return  contactService.vehicleConsultation(id);
    }
}
