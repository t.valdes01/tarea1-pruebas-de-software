package com.example.tarea1.controller;

import java.util.List;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.example.tarea1.model.Vehicle;
import com.example.tarea1.service.VehicleService;

@RestController
@RequestMapping("/vehicle")
public class VehicleController {
    private final VehicleService vehicleService;

    public VehicleController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @ApiResponse(description = "Create a specific quantity of vehicles")
    @GetMapping("/createVehicles")
    public List<Vehicle> createVehicle(
            @Parameter(description = "Quantity of vehicles to create")
            @RequestParam("quantity") int quantity) {
        return vehicleService.createVehicle(quantity);

    }

    
}
