package com.example.tarea1.service;

import com.example.tarea1.model.Vehicle;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ContactService {

    private final VehicleService vehicleService;

    public ContactService(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    /**
     * Consult for a specific vehicle
     * @param  id The id of the vehicle
     * @return vehicle
     */
    public Vehicle vehicleConsultation(int id){
     Optional<Vehicle> vehicle=vehicleService.getVehicleById(id);
     vehicle.get().increasePopularity();
     vehicleService.saveVehicle(vehicle.get());
     vehicle.get().setPopularity(null);
      return vehicle.get();
    }

}
