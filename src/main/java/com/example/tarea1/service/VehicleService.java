package com.example.tarea1.service;

import com.example.tarea1.model.*;
import com.example.tarea1.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;

@Service
public class VehicleService {

     final int VEHICLE_MIN_PRICE = 8000000;

     final int VEHICLE_MAX_PRICE = 30000000;
    Random random = new Random();
    private final VehicleRepository vehicleRepository;
    public VehicleService( VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    /**
     * Creates a list of random vehicles.
     * @return List of generated vehicles.
     */
    public List<Vehicle> createVehicle(int quantity) {
        List<Vehicle> vehicles = new ArrayList<>();

        for (int i = 0; i < quantity; i++) {
            Vehicle vehicle = vehicleTypeSetter();
            vehicle.setId(i);
            vehicle.setBrand(brandSetter());
            vehicle.setYear(yearSetter());
            vehicle.setColor(colorSetter());
            vehicle.setPrice(priceSetter());
            vehicle.setTurbo(turboSetter());
            vehicle.setVehicleType(vehicle.getVehicleType());
            vehicles.add(vehicle);
        }
        vehicleRepository.saveAll(vehicles);

        return vehicles;
    }

    /**
     * Randomly generates a value from an enum.
     * @param enumClass The class of the enum you want to get a random value from.
     * @return A random value from the enum.
     **/
    private <T> T randomGenerator(Class<T> enumClass) {
        T[] values = enumClass.getEnumConstants();
        int number = random.nextInt(values.length);
        return values[number];
    }

    /**
     * Randomly sets a mark for a vehicle.
     * @return A string representing the make of the vehicle.
     */
    private String brandSetter() {
        Brand brand = randomGenerator(Brand.class);
        return brand.toString();
    }

    /**
     * Randomly sets a year for a vehicle.
     * @return A string representing the year of the vehicle.
     */
    private String yearSetter() {
        Year year = randomGenerator(Year.class);
        return year.getYear();
    }

    /**
     * Randomly sets a color for a vehicle.
     * @return A string representing the color of the vehicle.
     */
    private String colorSetter() {
        Color color = randomGenerator(Color.class);
        return color.toString();
    }

    /**
     * Randomly sets a price for a vehicle, approximate to the thousands.
     * @return The price of the vehicle.
     */
    private int priceSetter() {
        return random.nextInt((VEHICLE_MAX_PRICE - VEHICLE_MIN_PRICE + 1) / 1000) * 1000 + VEHICLE_MIN_PRICE;
    }

    /**
     * Sets randomly whether a vehicle is turbocharged or not.
     * @return True if the vehicle has turbo, false otherwise.
     */
    private boolean turboSetter() {
        return random.nextBoolean();
    }

    /**
     * Sets randomly the type of vehicle created.
     * @return The type of vehicle created.
     */
    public Vehicle vehicleTypeSetter() {
        Random random = new Random();
        int randomType = random.nextInt(3);

        return switch (randomType) {
            case 0 -> createSedan();
            case 1 -> createVAN(random);
            case 2 -> createSUV(random);
            default -> null;
        };
    }

    /**
     * Creates a sedan vehicle.
     * @return A sedan vehicle.
     */
    private Sedan createSedan() {
        Sedan sedan = new Sedan();
        sedan.setEngine(Engine.getRandomEngineByBrand("Sedan"));
        return sedan;
    }

    /**
     * Creates a van vehicle.
     * @return A van vehicle.
     */
    private VAN createVAN(Random random) {
        VAN van = new VAN();
        van.setEngine(Engine.getRandomEngineByBrand("VAN"));
        van.setCabins(random.nextBoolean());
        return van;
    }

    /**
     * Creates a SUV vehicle.
     * @return a SUV vehicle.
     */
    private SUV createSUV(Random random) {
        SUV suv = new SUV();
        suv.setEngine(Engine.getRandomEngineByBrand("SUV"));
        suv.setSunroof(random.nextBoolean());
        return suv;
    }

    /**
     * Gets all the vehicles in the database.
     * @return A list of all the vehicles in the database.
     */
    public List<Vehicle> getAllVehicles(){
        return vehicleRepository.findAll();
    }

    /**
     * Gets a vehicle by its id.
     * @param id The id of the vehicle.
     * @return The vehicle with the given id.
     */
    public Optional<Vehicle> getVehicleById(int id){
        return vehicleRepository.findById(id);
    }

    /**
     * Saves a vehicle in the database.
     * @param vehicle The vehicle to be saved.
     */
    public void saveVehicle(Vehicle vehicle){
        vehicleRepository.save(vehicle);
    }

}
