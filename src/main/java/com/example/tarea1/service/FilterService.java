package com.example.tarea1.service;

import com.example.tarea1.model.Vehicle;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilterService {

    private final VehicleService vehicleService;

    public FilterService(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }


    /**
     * Filter vehicles by price, vehicle type and color
     * @param price The max price of the vehicle
     * @param vehicleType The type of the vehicle
     * @param color The color of the vehicle
     * @param userType The type of the user (agent, user)
     * @return List of filtered vehicles
     */
    public List<Vehicle> filterVehicles(Integer price, String vehicleType, String color, String userType) {
        List<Vehicle> filteredVehicles = applyFilters(price, vehicleType, color);

        increasePopularityForFilteredVehicles(filteredVehicles);

        if (!"agent".equalsIgnoreCase(userType)) {
            hidePopularityForFilteredVehicles(filteredVehicles);
        }

        return filteredVehicles;
    }

    /**
     * Apply filters to the list of vehicles
     * @param price The max price of the vehicle
     * @param vehicleType The type of the vehicle
     * @param color The color of the vehicle
     * @return List of filtered vehicles
     */
    private List<Vehicle> applyFilters(Integer price, String vehicleType, String color) {
        return vehicleService.getAllVehicles().stream()
                .filter(vehicle -> (price == null || price <= 0 || vehicle.getPrice() <= price))
                .filter(vehicle -> (vehicleType == null || vehicleType.isEmpty() || vehicleType.equalsIgnoreCase(vehicle.getVehicleType())))
                .filter(vehicle -> (color == null || color.isEmpty() || color.equalsIgnoreCase(vehicle.getColor())))
                .toList();
    }

    /**
     * Increase popularity for filtered vehicles
     * @param vehicles List of filtered vehicles
     */
    private void increasePopularityForFilteredVehicles(List<Vehicle> vehicles) {
        for (Vehicle vehicle : vehicles) {
            vehicle.increasePopularity();
            vehicleService.saveVehicle(vehicle);
        }
    }

    /**
     * Hide popularity for filtered vehicles
     * @param vehicles List of filtered vehicles
     */
    private void hidePopularityForFilteredVehicles(List<Vehicle> vehicles) {
        for (Vehicle vehicle : vehicles) {
            vehicle.setPopularity(null);
        }
    }


}
