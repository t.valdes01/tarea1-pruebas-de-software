
# Task 1

The software is an API for an automotive company where vehicles with random characteristics can be generated, and a filtering system for the generated vehicles is included.

The software is developed in Java 21, spring boot 3.1.4, a SQLite database and the implementation of swagger.

## Authors

- [@t.valdes01](https://gitlab.com/t.valdes01) - (Tomás Valdés)
- [@Daira0811](https://gitlab.com/Daira0811) - (Daira Acuña)

## Deployment

To deploy this project run

```bash
  npm run deploy
```


## Run Locally


### Requeriments
- Java 21



Clone the project

```bash
  git clone https://gitlab.com/t.valdes01/tarea1-pruebas-de-software.git
```

- Open the proyect in your IDE

- Setup the Java JDK in the project


To start the server run the file:

```bash
  Tarea1Application.java
```

The server now is running in:

```bash
  http://localhost:8080
```

You can access to swagger documentation and test the API in:
```bash
  http://localhost:8080/swagger-ui/index.html#
```
## How to use

The API has three endpoints.

### Vehicle

In this endpoint you can generate a selected quantity of vehicles with a randomized characteristics each one.
For that you need to insert the quantity in the parameters of the endpoint.

![img_2.png](src/main/resources/Images/img_2.png)

### Filter

In this you can filter the diferent vehicles generated in the previus endpoint by the characteristics type of vehicle, color and price.

The filter can be used for an "User" or an "Agent", then the vehicles filtered will increase them popularity by 1 and if the "User" is filtering the vehicles, the popularity will not be shown.

![img_4.png](src/main/resources/Images/img_4.png)
![img_3.png](src/main/resources/Images/img_3.png)

### Contact

In this endpoint you can consult for an specific vehicle through its ID, and then its popularity will be increase by 1.

![img_5.png](src/main/resources/Images/img_5.png)
